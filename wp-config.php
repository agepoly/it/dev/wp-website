<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'wordpress');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'jesus');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'dieu');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|B;yJ-{o_|pnj9MuI:P9pk6$/,}2<|WY?^~EN4E|m!~DCv,dE*|&3lATW.J wKdC');
define('SECURE_AUTH_KEY',  'mCyS7tOEewK,#&;qWFj&1=5l%vXcHLZv,?qJjkJ6WH!uy!-*)hYe#wTcj;qd_BU1');
define('LOGGED_IN_KEY',    'y96>JAu)cn+-7qr8NVBceh>_6UG>(v<YqrcimvP+,Gmt5]Y6}!SR_7C_[RQ2DVSg');
define('NONCE_KEY',        'ndH#n8S|&01C2(Z4=5CRKdDd;7Wh-,=1ta:Od$J8ShmT+!q*+X,k9=~P2Y.1>xu:');
define('AUTH_SALT',        'eWJV6,uU@Ut1&iCgqj#Nc:_c{IP#u@[$%xUaEo9J+d!S4sg<HQ[uwbS|0J=Ht1>|');
define('SECURE_AUTH_SALT', 'j!#B+BssRt#c:{70EasRtn>uz;;TBgoxSbFX=fr-#vuaB@A[l!J[F))]#,p#6sf~');
define('LOGGED_IN_SALT',   'V_d!5U ?-=v]hZMF|&3wKiDbi_[>h~:XRf<t!j2jcbJ=]-FvYkT-}JB 5e[cVcgE');
define('NONCE_SALT',       '#qmcR~gH*dc;WPc1z]:1}f<FJlJFBULa g5A`33o9IDekcD0!X%U$^Hm@X{,BQx)');
/**#@-*/

define('WP_HOME','http://128.179.196.128');
define('WP_SITEURL','http://128.179.196.128');

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

define('FS_METHOD', 'direct');


/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
