<?php

/* Menu */

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' )
     )
   );
 }
 add_action( 'init', 'register_my_menus' );

 ?>

 <?php

 /* Team */

 function generateTeam($name, $job, $mail, $pic) {?>
   <div class="col-6 col-lg-3 mb-7 mb-lg-0 space-1 cust_centered">
     <!-- Team -->
     <div class="text-center">
     <img class=" u-lg-avatar rounded-circle mx-auto mb-4" src="<?php echo $pic; ?>" alt="Portrait">
     <h4 class="h5"><?php echo $name; ?></h4>
     <span class="d-block text-primary"><?php echo $job; ?></span>
     <hr>
     <a class="text-secondary font-size-14" href="mailto:<?php echo $mail; ?>"><?php echo $mail; ?></a>
     </div>
     <!-- End Team -->
   </div>
   <?php
 }

 function startTeamSpace($title) {?>
   <h1 class="text-center"><?php echo $title; ?></h1>
   <div class="container">
       <div class="row mb-9 text-center"><?php
 }

 function endTeamSpace() {
   ?></div></div><?php
 }

 /* Cards */

 $cardcpt = 0;

 function generateCard($name, $desc, $site, $pic) {
   global $cardcpt;

   if($cardcpt == 0) {?>
    <div class="card-deck d-block d-lg-flex card-lg-gutters-2">
     <?php
   }

   ?>
   <div class="card border-0 mb-3">
     <div class="card-body border border-bottom-0 rounded-top text-center p-5">
     <img class="rounded mb-4" style="max-width: 256px; max-height:256px;" src="<?php echo $pic; ?>" alt="Icon">
     <h4 class="h5 mb-1"><?php echo $name; ?></h4>
     <p class="mb-0"><?php echo $desc; ?></p>
     </div>

     <div class="card-footer text-center border rounded-bottom p-5">
     <a class="btn btn-sm btn-primary btn-wide" href="<?php echo $site; ?>">Site Web</a>
     </div>
   </div>
   <?php

   $cardcpt++;

   if($cardcpt == 3) {
     $cardcpt = 0;
     ?>
    </div>
     <?php
   }
 }

 function startCardSpace($title) {?>
   <div class="container space-2 text-center">
 	 <h1 class="space-1-bottom"><?php echo $title; ?></h1><?php
 }

 function endCardSpace() {
   global $cardcpt;
   ?></div><?php
   $cardcpt = 0;
 }

 /* FAQ */

 $faqcpt = 0;
 $acccpt = 0;

 function startFaq($title) {
   global $acccpt; ?>
   <h2 align="center"><?php echo $title; ?></h2>
   <div id="basicsAccordion_<?php echo $acccpt; ?>" class="space-1"> <?php
 }

 function endFaq() {
   global $acccpt, $faqcpt;
   $acccpt++;
   $faqcpt = 0;
   ?> </div> <?php
 }

function addQuestion($question, $answer) {
  global $acccpt, $faqcpt; ?>
  <div class="card mb-3">
  <div class="card-header card-collapse__header" id="basicsHeading_<?php echo $acccpt; ?>_<?php echo $faqcpt; ?>">
    <h5 class="mb-0">
    <button class="btn btn-link btn-block d-flex justify-content-between card-collapse__btn <?php if ($faqcpt != 0) echo "collapsed"; ?> p-3"
        data-toggle="collapse"
        data-target="#basicsCollapse_<?php echo $acccpt; ?>_<?php echo $faqcpt; ?>"
        aria-expanded="<?php echo ($faqcpt == 0 ? "true" : "false"); ?>"
        aria-controls="basicsCollapse_<?php echo $acccpt; ?>_<?php echo $faqcpt; ?>">
      <?php echo $question; ?>

      <span class="card-collapse__btn-arrow">
      <span class="fa fa-arrow-down small"></span>
      </span>
    </button>
    </h5>
  </div>
  <div id="basicsCollapse_<?php echo $acccpt; ?>_<?php echo $faqcpt; ?>" class="collapse <?php if ($faqcpt == 0) echo "show"; ?>"
     aria-labelledby="basicsHeading_<?php echo $acccpt; ?>_<?php echo $faqcpt; ?>"
     data-parent="#basicsAccordion_<?php echo $acccpt; ?>">
    <div class="card-body card-collapse__body">
    <?php echo $answer; ?>
    </div>
  </div>
</div> <?php
  $faqcpt++;
}

  ?>
