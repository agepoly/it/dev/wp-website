<?php

$rnd = rand(1,9);
$hero = "url(./wp-content/themes/agepoly/resources/agep_banner_$rnd.jpg)";
$lang = function_exists("pll_current_language") ? (pll_current_language() != "en") : true; //true is fr_FR and false is en_GB

?>

<main id="content" role="main">
  <!-- Hero Section -->
  <div class="gradient-overlay-half-dark-v1 bg-img-hero" style="background-image: <?php echo $hero ?>;">
    <div class="d-lg-flex align-items-lg-center flex-lg-column">
      <div class="container space-3 space-4-top--lg">
        <!-- Title -->
        <div class="w-md-50">
          <h1 class="display-4 font-size-48--md-down text-white">AGEPoly</h1>
          <p class="lead text-white"><?php echo $lang ? "Association Générale des Étudiants de l'EPFL" : "General Students' Association of EPFL"; ?></p>
        </div>
        <!-- End Title -->
      </div>
    </div>

    <div class="container space-1-bottom">
      <!-- Fancybox -->
      <div class="d-inline-block">
        <a class="js-fancybox u-media-player media align-items-center text-white" href="javascript:;"
           data-src="//www.youtube.com/watch?v=8avbLNgtebk&feature=youtu.be"
           data-speed="700"
           data-animate-in="zoomIn"
           data-animate-out="zoomOut"
           data-caption="<?php echo $lang ? "L'AGEPoly : qu'est-ce que c'est ?" : "What is AGEPoly?"; ?>">
          <span class="u-media-player__icon mr-3">
            <span class="fa fa-play u-media-player__icon-inner"></span>
          </span>
          <span class="media-body">
            <small class="d-block text-uppercase">Video</small>
            <?php echo $lang ? "L'AGEPoly : qu'est-ce que c'est ?" : "What is AGEPoly?"; ?>
          </span>
        </a>
      </div>
      <!-- End Fancybox -->
    </div>
  </div>
  <!-- End Hero Section -->

<!-- Features Section -->
<div class="container text-center space-2">
  <div class="row">
  <div class="col-sm-6 col-lg-4 offset-sm-3 offset-lg-0 order-lg-2 mb-7 mb-sm-0">
    <!-- Icon Block -->
    <div class="bg-primary text-center rounded py-9 p-5">
    <img class="max-width-10 mb-2" src="./wp-content/themes/agepoly/resources/icon-shopping-cart-white.svg" alt="Image Description">
    <h2 class="h4 text-white"><?php echo $lang ? "La Boutique" : "Our Shop"; ?></h2>
    <p class="text-white"><?php echo $lang ? "Vêtements EPFL & AGEPoly, recharge camipro, goodies..." : "EPFL & AGEPoly clothes, camipro reloading, goodies..."; ?></p>
    <a class="text-white" href="<?php echo $lang ? "./?page_id=83" : "/?page_id=208&lang=en"; ?>">
      <?php echo $lang ? "Plus d'infos" : "More info"; ?>
      <span class="fa fa-angle-right align-middle ml-2"></span>
    </a>
    </div>
    <!-- End Icon Block -->
  </div>

  <div class="col-sm-6 col-lg-4 order-lg-1">
    <!-- Icon Block -->
    <div class="text-center py-sm-9 p-5">
    <img class="max-width-10 mb-2" src="./wp-content/themes/agepoly/resources/icon-users-agepblue_1.svg" alt="Image Description">
    <h2 class="h4 text-dark"><?php echo $lang ? "Le Comité" : "The Committee"; ?></h2>
    <p><?php echo $lang ? "A ton service tout au long de l'année !" : "At your servive all year long!"; ?></p>
    <a href="<?php echo $lang ? "./?page_id=52" : "/?page_id=210&lang=en"; ?>">
      <?php echo $lang ? "Plus d'infos" : "More info"; ?>
      <span class="fa fa-angle-right align-middle ml-2"></span>
    </a>
    </div>
    <!-- End Icon Block -->
  </div>

  <div class="col-sm-6 col-lg-4 order-lg-3">
    <!-- Icon Block -->
    <div class="text-center py-sm-9 p-5">
    <img class="max-width-10 mb-2" src="./wp-content/themes/agepoly/resources/icon-star-agepblue_1.svg" alt="Image Description">
    <h2 class="h4 text-dark"><?php echo $lang ? "Bons Plans" : "Good Deals"; ?></h2>
    <p><?php echo $lang ? "Des offres exclusives proposées par nos partenaires !" : "Exclusive offers proposed by our partners"; ?></p>
    <a href="<?php echo $lang ? "./?page_id=116" : "/?page_id=226&lang=en"; ?>">
      <?php echo $lang ? "Plus d'infos" : "More info"; ?>
      <span class="fa fa-angle-right align-middle ml-2"></span>
    </a>
    </div>
    <!-- End Icon Block -->
  </div>
  </div>
</div>
<!-- End Features Section -->

<!-- Blog-->

<?php

if (have_posts()) { ?>
  <div class="container text-center">
  <h3 class="space-1-bottom">Blog</h3>
  <div class="card-deck d-block d-lg-flex">
  <?php
  include $ROOT.'/agepoly/html/thumb_blog.php';

  if (have_posts()) {
    include $ROOT.'/agepoly/html/thumb_blog.php';
  }
?>
  </div>
</div> <?php
}

 ?>

<!-- End Blog -->

<!-- Instagram -->

<div class="container text-center space-1-bottom">
  <h3 class="space-1-bottom">Instagram</h3>
  <div id="instaFeed" class="js-instagram row mx-gutters-2 container"
  data-user-id="1818204499"
  data-client-id="cc9ed44607064cdebe2e071bc86ea9da"
  data-token="1818204499.cc9ed44.4a1a13934ed44a4eb9ef3f8ac22437bf"
  data-limit="4"
  data-template='<div class="col-md-3 cust_centered"><a href="{{link}}" target="_blank"><img class="img-fluid w-100 rounded" src="{{image}}" /></a></div>'>
  </div>
</Sdiv>

</main>
