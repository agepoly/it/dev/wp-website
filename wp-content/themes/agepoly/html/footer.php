<?php

$lang = function_exists("pll_current_language") ? (pll_current_language() != "en") : true; //true is fr_FR and false is en_GB

?>


<html>
<footer class="bg-dark">
  <div class="container space-2">
    <div class="row justify-content-md-between">
      <div class="col-6 col-md-3 col-lg-2 order-lg-3 mb-7 mb-lg-0">
        <h3 class="h6 text-white mb-3">AGEPoly</h3>

        <!-- List Group -->
        <div class="list-group list-group-flush list-group-transparent">
          <a class="list-group-item list-group-item-action" href="<?php echo $lang ? "./?page_id=52" : "/?page_id=210&lang=en"; ?>"><?php echo $lang ? "Comité & Collaborateurs" : "Committee & Employees"; ?></a>
          <a class="list-group-item list-group-item-action" href="<?php echo $lang ? "./?page_id=83" : "/?page_id=208&lang=en"; ?>"><?php echo $lang ? "Boutique" : "Shop"; ?></a>
          <a class="list-group-item list-group-item-action" href="<?php echo $lang ? "./?page_id=50" : "/?page_id=214&lang=en"; ?>"><?php echo $lang ? "Documents officiels" : "Official documents"; ?></a>
          <a class="list-group-item list-group-item-action" href="<?php echo $lang ? "./?page_id=86" : "/?page_id=206&lang=en"; ?>"><?php echo $lang ? "Assemblées Générales" : "General Assemblies"; ?></a>
        </div>
        <!-- End List Group -->
      </div>

      <div class="col-6 col-md-3 col-lg-2 order-lg-5 mb-7 mb-lg-0">
        <h3 class="h6 text-white mb-3"><?php echo $lang ? "Autres" : "Miscellaneous"; ?></h3>

        <!-- List Group -->
        <div class="list-group list-group-flush list-group-transparent">
          <a class="list-group-item list-group-item-action" href="<?php echo $lang ? "./?page_id=83" : "/?page_id=208&lang=en"; ?>">Contact</a>
          <a class="list-group-item list-group-item-action" href="<?php echo $lang ? "./?page_id=81" : "/?page_id=218&lang=en"; ?>">FAQ</a>
          <a class="list-group-item list-group-item-action" href="./?page_id=278"><?php echo $lang ? "Mentions Légales" : "Legal Mentions"; ?></a>
        </div>
        <!-- End List Group -->
      </div>

      <div class="col-6 col-md-3 col-lg-2 order-lg-6 mb-7 mb-lg-0">
        <h3 class="h6 text-white mb-3">Social</h3>

        <!-- List -->
        <div class="list-group list-group-flush list-group-transparent">
          <a class="list-group-item list-group-item-action" href="https://www.facebook.com/AGEPoly/">
            <span class="fab fa-facebook-f min-width-3 text-center mr-2"></span>
            Facebook
          </a>
          <a class="list-group-item list-group-item-action" href="https://twitter.com/agepoly?lang=fr">
            <span class="fab fa-twitter min-width-3 text-center mr-2"></span>
            Twitter
          </a>
          <a class="list-group-item list-group-item-action" href="https://www.instagram.com/agepoly/">
            <span class="fab fa-instagram min-width-3 text-center mr-2"></span>
            Instagram
          </a>
        </div>
        <!-- End List -->
      </div>

      <div class="col-lg-4 order-lg-1 d-flex align-items-start flex-column">
        <!-- Logo -->
    <div class="d-inline-block">
        <a class="d-inline-block mb-5" href="index.html" aria-label="Space">
          <img src="./wp-content/themes/agepoly/resources/Vertical_-_AI_-_AGEPoly.svg" alt="Logo" style="width: 40px; max-width: 100%;">
        </a>
    <a class="d-inline-block mb-5 pl-2 text-secondary" href="https://tequila.epfl.ch/" aria-label="Space">Connexion</a>
    </div>
        <!-- End Logo -->

        <!-- Language -->
        <div class="btn-group d-block position-relative mb-4 mb-lg-auto">
            <?php if (function_exists("pll_the_languages")) {
              $langs = pll_the_languages(array("raw" => 1));
              foreach ($langs as $lang) {
                if (!($lang["current_lang"])) { ?>
                  <a id="footerLanguageInvoker" class="btn-text-secondary d-flex align-items-center u-unfold--language-btn rounded py-2 px-3" href="<?php echo $lang["url"]; ?>" role="button">
                  <img class="max-width-3 mr-2" src="<?php echo $lang["flag"]; ?>" alt="Lang Flag">
                  <span class="font-size-14"><?php echo $lang["name"]; ?></span>
                <?php }
              }
            } ?>
          </a>
        </div>
        <!-- End Language -->

        <p class="small text-muted"><?php echo $lang ? "Tous droits réservés" : "All rights reserved"; ?> &copy; AGEPoly 2019.</p>
      </div>
    </div>
  </div>
</footer>
<!-- ========== END FOOTER ========== -->

<!-- ========== SECONDARY CONTENTS ========== -->

<!-- ========== END SECONDARY CONTENTS ========== -->

<!-- Go to Top -->
<a class="js-go-to u-go-to" href="javascript:;"
  data-position='{"bottom": 15, "right": 15 }'
  data-type="fixed"
  data-offset-top="400"
  data-compensation="#header"
  data-show-effect="slideInUp"
  data-hide-effect="slideOutDown">
  <span class="fa fa-arrow-up u-go-to__inner"></span>
</a>
<!-- End Go to Top -->

<!-- JS Global Compulsory -->
<script src="./wp-content/themes/agepoly/assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="./wp-content/themes/agepoly/assets/vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
<script src="./wp-content/themes/agepoly/assets/vendor/popper.js/dist/umd/popper.min.js"></script>
<script src="./wp-content/themes/agepoly/assets/vendor/bootstrap/bootstrap.min.js"></script>

<!-- JS Implementing Plugins -->
<script src="./wp-content/themes/agepoly/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
<script src="./wp-content/themes/agepoly/assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="./wp-content/themes/agepoly/assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="./wp-content/themes/agepoly/assets/vendor/custombox/dist/custombox.min.js"></script>
<script src="./wp-content/themes/agepoly/assets/vendor/custombox/dist/custombox.legacy.min.js"></script>
<script src="./wp-content/themes/agepoly/assets/vendor/slick-carousel/slick/slick.js"></script>
<script src="./wp-content/themes/agepoly/assets/vendor/fancybox/jquery.fancybox.min.js"></script>
<script src="./wp-content/themes/agepoly/assets/vendor/player.js/dist/player.min.js"></script>
<script src="./wp-content/themes/agepoly/assets/vendor/instafeed.js/instafeed.min.js"></script>

<!-- JS Space -->
<script src="./wp-content/themes/agepoly/assets/js/hs.core.js"></script>
<script src="./wp-content/themes/agepoly/assets/js/components/hs.header.js"></script>
<script src="./wp-content/themes/agepoly/assets/js/components/hs.unfold.js"></script>
<script src="./wp-content/themes/agepoly/assets/js/components/hs.validation.js"></script>
<script src="./wp-content/themes/agepoly/assets/js/helpers/hs.focus-state.js"></script>
<script src="./wp-content/themes/agepoly/assets/js/components/hs.malihu-scrollbar.js"></script>
<script src="./wp-content/themes/agepoly/assets/js/components/hs.modal-window.js"></script>
<script src="./wp-content/themes/agepoly/assets/js/components/hs.show-animation.js"></script>
<script src="./wp-content/themes/agepoly/assets/js/components/hs.slick-carousel.js"></script>
<script src="./wp-content/themes/agepoly/assets/js/components/hs.video-player.js"></script>
<script src="./wp-content/themes/agepoly/assets/js/components/hs.fancybox.js"></script>
<script src="./wp-content/themes/agepoly/assets/js/components/hs.go-to.js"></script>
<script src="./wp-content/themes/agepoly/assets/js/components/hs.instagram.js"></script>

<!-- JS Plugins Init. -->
<script>
  $(window).on('load', function () {
    // initialization of HSMegaMenu component
    $('.js-mega-menu').HSMegaMenu({
      event: 'hover',
      pageContainer: $('.container'),
      breakpoint: 991,
      hideTimeOut: 0
    });
  });

  $(document).on('ready', function () {
    // initialization of header
    $.HSCore.components.HSHeader.init($('#header'));

    // initialization of unfold component
    $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
      afterOpen: function () {
        if (!$('body').hasClass('IE11')) {
          $(this).find('input[type="search"]').focus();
        }
      }
    });

    // initialization of form validation
    $.HSCore.components.HSValidation.init('.js-validate', {
      rules: {
        confirmPassword: {
          equalTo: '#password'
        }
      }
    });

    // initialization of forms
    $.HSCore.helpers.HSFocusState.init();

    // initialization of malihu scrollbar
    $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

    // initialization of autonomous popups
    $.HSCore.components.HSModalWindow.init('[data-modal-target]', '.js-signup-modal', {
      autonomous: true
    });

    // initialization of show animations
    $.HSCore.components.HSShowAnimation.init('.js-animation-link');

    // initialization of slick carousel
    $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

    // initialization of fancybox
    $.HSCore.components.HSFancyBox.init('.js-fancybox');

    // initialization of video player
    $.HSCore.components.HSVideoPlayer.init('.js-inline-video-player');

    // initialization of go to
    $.HSCore.components.HSGoTo.init('.js-go-to');
  });
</script>
<script>
  $(document).on('ready', function () {
  // initialization of instagram api
  $.HSCore.components.HSInstagram.init('#instaFeed', {
    resolution: 'standard_resolution',
    after: function () {
    // initialization of masonry.js
    var $grid = $('.js-instagram').masonry({
      percentPosition: true
    });

    // initialization of images loaded
    $grid.imagesLoaded().progress(function () {
      $grid.masonry();
    });
    }
  });
  });
</script>
</body>
</html>
