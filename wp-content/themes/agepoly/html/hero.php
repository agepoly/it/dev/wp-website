<?php

$rnd = rand(1,9);
$hero = "url(./wp-content/themes/agepoly/resources/agep_banner_$rnd.jpg)";

?>

<div id="hero_bg" class="gradient-overlay-half-dark-v2 bg-img-hero" style="background-image: <?php echo $hero ?>;">
  <div class="container space-2 space-4-top--lg space-3-bottom--lg">
  <div class="w-lg-60 text-center mx-lg-auto">
    <h1 class="display-2 font-size-48--md-down text-white mb-0"><?php the_title(); ?></h1>
  </div>
  </div>
</div>

<script>
  function set_hero(url) {
    document.getElementById("hero_bg").style.backgroundImage = url;
  }
</script>
